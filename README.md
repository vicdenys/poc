# POC - solo generator #

The solo generator will try to create a musical fragment based on a played melody by a user.

## 1. Research ##

### 1.1 A.I. Duet - Google ###

>>This experiment lets you make music through machine learning. A neural network was trained on many example >> melodies, and it learns about musical concepts, building a map of notes and timings. You just play a few notes, and see how the neural net responds. We’re working on putting the experiment on the web so that anyone can play with it. In the meantime, you can get the code and learn about it by watching the video above.

It uses [Tone.js](https://github.com/Tonejs/Tone.js) and [the Magenta project](https://github.com/tensorflow/magenta).


### 1.2 MIDI to the browser ###

MIDI is only supported on Google Chrome and Opera (2017 jan).

##### 1.2.1 [WebMIDI.js](https://github.com/cotejp/webmidi) #####

>>WebMidi.js helps you tame the Web MIDI API. Send and receive MIDI messages with ease. Control instruments with user-friendly functions (playNote, sendPitchBend, etc.). React to MIDI input with simple event listeners (noteon, pitchbend, controlchange, etc.).

##### 1.2.1 [Tone.js](https://github.com/Tonejs/Tone.js) #####
>>Tone.js is a Web Audio framework for creating interactive music in the browser. The architecture of Tone.js aims to be familiar to both musicians and audio programmers looking to create web-based audio applications. On the high-level, Tone offers common DAW (digital audio workstation) features like a global transport for scheduling events and prebuilt synths and effects. For signal-processing programmers (coming from languages like Max/MSP), Tone provides a wealth of high performance, low latency building blocks and DSP modules to build your own synthesizers, effects, and complex control signals.

### 1.3 Neural networks ###

##### 1.2.1 [Synaptic](https://github.com/cazala/synaptic) #####

>>WebMidi.js helps you tame the Web MIDI API. Send and receive MIDI messages with ease. Control instruments with user-friendly functions (playNote, sendPitchBend, etc.). React to MIDI input with simple event listeners (noteon, pitchbend, controlchange, etc.).


## 2. Process ##

### 2.1 Connecting MIDI device and record fragment ###

### 2.2 Create and Train Network ###

### 2.3 Saving Network to server ###

### 2.3 Output Networks melody ###

## 3. Documentation ##
[Neural Networks](https://blog.webkid.io/neural-networks-in-javascript/)
[Neural Networks 2](https://github.com/cazala/synaptic/wiki/Neural-Networks-101)
[Music composition by neural networks](http://www.hexahedria.com/2015/08/03/composing-music-with-recurrent-neural-networks/)
[Google A.I. Duet](https://aiexperiments.withgoogle.com/ai-duet)
