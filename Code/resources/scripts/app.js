(function() {

  var webMidi = require('webmidi');
  var Tone = require('Tone');
  var MidiUtils = require('midiutils');
  var Synaptic = require('synaptic');

  var midiInput,
  synth,
  playedNote,
  playedNoteOctave,
  noteStartTime,
  sequenceStartTime,
  timer,
  trainerInput,
  trainerOutput,
  trainer,
  myLSTM;

  var playedSequence = [];
  var isFirst = true;
  var isProcessing = false;
  var isPlaying = false;
  var counter = 0;
  var isTraining = false;
  var isTrainingInput = false;
  var trainerData = [];
  var isSaving = false;
  var isLoading = true;

  var SEQUENCE_MAX_TIME_INTERVAL = 2000;

  var trainerBtn = document.getElementById('trainerBtn');
  var trainerText = document.getElementById('trainerText');
  var saveBtn = document.getElementById('saveBtn');

  //-------------------------------
  // Initialize Tone
  //-------------------------------
  synth = new Tone.Synth({
    "oscillator" : {
      "type" : "sine",
      "modulationFrequency" : 0.2
    },
    "envelope" : {
      "attack" : 0.02,
      "decay" : 0.1,
      "sustain" : 0.2,
      "release" : 0.9,
    }
  }).toMaster();

  //-------------------------------
  // Initialize webmidi
  //-------------------------------
  webMidi.enable(function (err) {
    if (err) {
      console.log("WebMidi could not be enabled.", err);
    }else {
      console.log("WebMidi enabled!");

      // Set midiInput and add listener
      midiInput = webMidi.inputs[0];
      midiInput.addListener('noteon', "all", onKeyPress);
      midiInput.addListener('noteoff', "all", onKeyUp);
    }
  });

  //-------------------------------
  // Initialize Network
  //-------------------------------
  var getHTTP = new XMLHttpRequest();
  getHTTP.onload = function() {
      if (getHTTP.readyState == XMLHttpRequest.DONE ) {
         if (getHTTP.status == 200) {
              if(getHTTP.responseText){
                myLSTM = Synaptic.Network.fromJSON(JSON.parse(getHTTP.responseText));
                console.log(myLSTM);
                isLoading = false;
              }
         }
         else if (getHTTP.status == 400) {
            alert('There was an error 400');
         }
         else {
             alert('something else other than 200 was returned');
         }
      }
  };
  getHTTP.open('GET', '/getnetwork', true);
  getHTTP.send();

  //--------------------------------------------------
  // Start timer to check if user's done playing sequence
  //--------------------------------------------------
  timer = setInterval(tick, 500);

  //-------------------------------
  // Event Functions for key inputs
  //-------------------------------
  function onKeyPress(e){
    if(!isLoading){
      counter = 0;
      isPlaying = true;
      synth.triggerRelease();

      if (!isProcessing && !isSaving){
        playedNoteOctave = MidiUtils.noteNumberToName(e.note.number);

        if(isFirst){
          sequenceStartTime = Tone.now();
          isFirst = false;
        }

        noteStartTime = Tone.now();

        synth.triggerAttack(playedNoteOctave.replace('-', ''));
      }
    }
  }

  function onKeyUp(e){
    if(!isLoading){
      synth.triggerRelease();
      counter = 0;
      isPlaying = false;

      if (!isProcessing && !isSaving){

        var noteEndTime = Math.round((Tone.now() - noteStartTime) * 1000);
        var noteStart = Math.round((noteStartTime - sequenceStartTime) * 1000);

        playedNote = {
          note: playedNoteOctave.substring(0,1),
          octave: playedNoteOctave.substring(2,3),
          time: noteEndTime,
          startTime: noteStart
        };

        playedSequence.push(playedNote);
      }
    }
  }

  function tick() {
    if (!isProcessing){
      counter += 500;

      if(counter >= SEQUENCE_MAX_TIME_INTERVAL && !isFirst && !isPlaying || isSaving ){
        clearInterval(timer);

        if(!isSaving){
          processSequence();
        }
      }
    }
  }

  //-------------------------------
  // Enable Train mode
  //-------------------------------
  trainerBtn.addEventListener("click", function () {
    isTraining = true;
    isTrainingInput = true;

    trainerBtn.style.display = 'none';
    trainerText.innerHTML = 'Play input sequence and wait 2s';
    trainerText.style.display = 'block';
  });

  //-------------------------------
  // Save Training data to server
  //-------------------------------
  saveBtn.addEventListener("click", function () {
    saveBtn.disabled = true;
    isSaving = true;

    var data = {
      trainingData: trainerData
    };
    JSONReq('POST', '/trainer', data, function () {
      console.log('Saved Trainer');
      startTraining();
    });

  });


  //-------------------------------
  // Proces Sequence
  //-------------------------------
  function processSequence() {
    if(!isProcessing){
      isProcessing = true;

      // normalize sequence
      playedSequence = normalizeSequence(playedSequence);

      if(isTraining){
        if(isTrainingInput){
          trainerInput = playedSequence;
          isTrainingInput = false;

          trainerText.innerHTML = 'Now play output sequence and wait 2s';
          saveBtn.style.display = 'none';
        }
        else{
          trainerOutput = playedSequence;

          trainerData.push({
            input: trainerInput,
            output: trainerOutput
          });

          isTrainingInput = true;
          trainerText.innerHTML = 'Play input sequence again and wait 2s';
          saveBtn.style.display = 'inline-block';
        }

        resetVariables();
      }else{
        processGeneratedSequence(myLSTM.activate(playedSequence));
      }

    }
  }

  function resetVariables() {
      //reset Timer
      timer = setInterval(tick, 500);
      isProcessing = false;
      isPlaying = false;
      isFirst = true;
      counter = 0;
      playedSequence = [];
  }

  //------------------------------------------------------
  // Normalize sequance information (data between 0 and 1)
  //------------------------------------------------------
  function normalizeSequence(sequence) {
    var normalizedSequence = [];

    for(var note in sequence){
      normalizedSequence.push.apply(normalizedSequence, normalizeNoteName(sequence[note].note));
      normalizedSequence.push(sequence[note].octave / 10);
      normalizedSequence.push(sequence[note].time / 10000);
      normalizedSequence.push(sequence[note].startTime / 10000);
    }

    return normalizedSequence;
  }


  //-------------------------------
  // process the generated request
  //-------------------------------
  function processGeneratedSequence(sequence) {
    var note;
    var noteArray = [];
    var generatedSequence = [];

    for (var i = 0; i < 5; i++){
      var indexMultiplier = 7 * i;

      for (var ii = 0; ii < 4; ii++){
        noteArray.push(Math.round(sequence[indexMultiplier + ii]));
      }

      note = {
        note: getNoteName(noteArray),
        time : Math.round(sequence[indexMultiplier + 5] * 10000),
        octave: Math.round(sequence[indexMultiplier + 4] * 10),
        startTime: Math.round(sequence[indexMultiplier + 6] * 10000)
      };
      generatedSequence.push(note);

      noteArray = [];
      note = {};
    }

    playGeneratedSequence(generatedSequence);
  }

  //-------------------------------
  // play the generated request
  //-------------------------------
  function playGeneratedSequence(generatedSequence) {

    var part = new Tone.Part(function(time, value){
    	//the value is an object which contains both the note and the velocity
    	synth.triggerAttackRelease(value.note, "8n", time, value.velocity);
    }, [
      {"time" : "0:0:2", "note" : generatedSequence[0].note + generatedSequence[0].octave, "velocity": 1},
    	{"time" : "0:1", "note" : generatedSequence[1].note + generatedSequence[1].octave, "velocity": 1},
      {"time" : "0:1:2", "note" : generatedSequence[2].note + generatedSequence[2].octave, "velocity": 1},
      {"time" : "0:2", "note" : generatedSequence[3].note + generatedSequence[3].octave, "velocity": 1},
      {"time" : "0:2:2", "note" : generatedSequence[4].note + generatedSequence[4].octave, "velocity": 1}
    ]).start(0);

    Tone.Transport.start();
    resetVariables();
  }

  //-------------------------------
  // Start The Trainer
  //-------------------------------
  function startTraining() {
    console.log(trainerData);
    myLSTM = new Synaptic.Architect.LSTM(35,35,35,35);
    trainer = new Synaptic.Trainer(myLSTM);
    trainer.train(trainerData, {
        rate: 0.001,
        iterations: 400,
        error: 0.005,
        shuffle: true,
        log: 100,
        cost: Synaptic.Trainer.cost.CROSS_ENTROPY,
        schedule: {
            every: 400,
            do: function(data) {
                // custom log
                console.log("error", data.error, "iterations", data.iterations, "rate", data.rate);
                if (data.iterations >= 400){
                  testNetwork();
                  return true; // abort/stop training
                }

            }
        }
    });
  }

  //-------------------------------
  // Test Network
  //-------------------------------
  function testNetwork() {
    saveNetwork();
  }

  //-------------------------------
  // Save network state to server
  //-------------------------------
  function saveNetwork() {
      var data = myLSTM.toJSON();
      JSONReq('POST', '/network', data, function () {
        console.log('Saved Network');
      });
  }

  function JSONReq(type, url, data, callback) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onload = function() {
        if (xmlhttp.readyState == XMLHttpRequest.DONE ) {
           if (xmlhttp.status == 200) {
                callback(xmlhttp.responseText);
           }
           else if (xmlhttp.status == 400) {
              alert('There was an error 400');
           }
           else {
               alert('something else other than 200 was returned');
           }
        }
    };
    xmlhttp.open(type, url, true);
    xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xmlhttp.send(JSON.stringify(data));
  }


    //-------------------------------
    // Returns binary of notename
    //-------------------------------
    function normalizeNoteName(note) {
      noteBin = [];
      switch(note) {
        case 'C':
          noteBin = [0,0,0,0];
          break;
        case 'C#':
          noteBin = [0,0,0,1];
          break;
        case 'D':
          noteBin = [0,0,1,0];
          break;
        case 'D#':
          noteBin = [0,0,1,1];
          break;
        case 'E':
          noteBin = [0,1,0,0];
          break;
        case 'F':
          noteBin = [0,1,0,1];
          break;
        case 'F#':
          noteBin = [0,1,1,0];
          break;
        case 'G':
          noteBin = [0,1,1,1];
          break;
        case 'G#':
          noteBin = [1,0,0,0];
          break;
        case 'A':
          noteBin = [1,0,0,1];
          break;
        case 'A#':
          noteBin = [1,0,1,0];
          break;
        case 'B':
          noteBin = [1,0,1,1];
          break;
        default:
          noteBin = [0,0,0,0];
      }
      return noteBin;
    }


      //-------------------------------
      // Returns notename of binary
      //-------------------------------
      function getNoteName(noteArray) {
        var noteArrayString = noteArray.join(' ');
        var note = '';

        switch(noteArrayString) {
          case '0 0 0 0':
            note = 'C';
            break;
          case '0 0 0 1':
            note = 'C#';
            break;
          case '0 0 1 0':
            note = 'D';
            break;
          case '0 0 1 1':
            note = 'D#';
            break;
          case '0 1 0 0':
            note = 'E';
            break;
          case '0 1 0 1':
            note = 'F';
            break;
          case '0 1 1 0':
            note = 'F#';
            break;
          case '0 1 1 1':
            note = 'G';
            break;
          case '1 0 0 0':
            note = 'G#';
            break;
          case '1 0 0 1':
            note = 'A';
            break;
          case '1 0 1 0':
            note = 'A#';
            break;
          case '1 0 1 1':
            note = 'B';
            break;
          default:
            note = 'C';
        }
        return note;
      }


})();
