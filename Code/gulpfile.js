/**
 * Basic gulp file that does bower installs, uses font-awesome,
 * 		and translates sass to css.
 */
var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefix = require('gulp-autoprefixer');
var notify = require("gulp-notify");
var bower = require('gulp-bower');
var browserify = require('gulp-browserify');
/**
 * Quick access variables for directory names.
 */
var config = {
	bootstrapDir: './bower_components/bootstrap-sass',
	fontawesomeDir: './bower_components/font-awesome',
	publicDir: './public',
  resourcesDir: './resources',
	bowerDir: './bower_components'
};

/**
 * Task for installing new bower dependencies inside bower.json.
 */
gulp.task('bower', function() {
	return bower()
		.pipe(gulp.dest(config.bowerDir));
});

/**
 * Task for moving font-awesome fonts inside the /public/fonts folder.
 */
gulp.task('fonts', function() {
	return gulp.src(config.bowerDir + '/font-awesome/fonts/**.*')
		.pipe(gulp.dest('./public/fonts'));
});

/**
 * Task for translating sass inside the /public/sass folder to css in the
 *		/public/stylesheets folder.
 */
gulp.task('css', function() {
	return gulp.src(config.resourcesDir + '/styles/sass/app.scss')
		.pipe(sass({
			includePaths: [config.bootstrapDir + '/assets/stylesheets', config.fontawesomeDir + '/scss'],
		}))
		.pipe(gulp.dest(config.publicDir + '/css'));
});

/**
 * Task for translating js inside the /resource/js folder to js in the
 *		/public/js folder.
 */
gulp.task('script', function() {
	return gulp.src(config.resourcesDir + '/scripts/app.js')
        .pipe(browserify({
          insertGlobals : true,
          debug : !gulp.env.production
        }))
        .pipe(gulp.dest(config.publicDir + '/js'));
});

/**
 * Watch task that looks for .scss file changes inside the /public/sass folder.
 * `gulp watch`.
 */
gulp.task('watch',function() {
	gulp.watch(config.resourcesDir + '/styles/sass/**/*.scss', ['css']);
	gulp.watch(config.resourcesDir + '/scripts/**/*.js', ['script']);
});

/**
 * When using `gulp`, bower, fonts, and css tasks are triggered.
 * `gulp`
 */
gulp.task('default', ['bower', 'fonts', 'css', 'script']);
