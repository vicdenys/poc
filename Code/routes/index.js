var express = require('express');
var router = express.Router();
var jsonfile = require('jsonfile');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Play music' });
});

router.post('/trainer', function(req, res, next) {
  var file = './data/trainer.json';
  var obj = req.body;

  jsonfile.writeFile(file, obj, function (err) {
      console.error(err);
  });

  jsonfile.readFile(file, function(err, obj) {
    obj = obj;
  });

  res.send(obj);
});

router.post('/network', function(req, res, next) {
  var file = './data/network.json';
  var obj = req.body;

  jsonfile.writeFile(file, obj, function (err) {
      console.error(err);
  });

  jsonfile.readFile(file, function(err, obj) {
    obj = obj;
  });

  res.send(obj);
});

router.get('/getnetwork', function(req, res, next) {
  var file = './data/network.json';
  var data;

  jsonfile.readFile(file, function(err, obj) {
    data = obj;
    res.send(data);
  });
});

module.exports = router;
